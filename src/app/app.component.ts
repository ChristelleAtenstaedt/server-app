import { Component } from '@angular/core';

@Component({
  selector: 'app-root', // selector property which assigns a string as a value which holds app-root, same text as in index.html. the app.component.html is injected via this tag.
  templateUrl: './app.component.html',
  // styleUrls: ['./app.component.css'],
  styles: [
    `
      h3 {
        color: dodgerblue;
      }
    `,
  ],
})
export class AppComponent {
  name = 'Max';
}

// 'selector' has to be unique
