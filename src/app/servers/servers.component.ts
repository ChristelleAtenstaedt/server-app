import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-servers',
  // selector: '[app-servers]', // here we have changed app-servers selector to an attribute.
  // selector: '.app-servers', // here we have changed app-servers to a class
  // template: ` <app-server></app-server>
  //   <app-server></app-server>`,
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css'],
})
export class ServersComponent implements OnInit {
  //another property using a boolean value
  allowNewServer = false;
  serverCreationStatus = 'No server was created';
  serverName = 'Test server';
  userName = '';
  serverCreated = false;
  servers = ['Testserver', 'Testserver 2'];
  showSecret = false;
  log = [];
  //method executed at the point of time this component created by Angular
  constructor() {
    setTimeout(() => {
      this.allowNewServer = true;
    }, 2000);
  }

  ngOnInit() {}

  onCreateServer() {
    this.serverCreated = true;
    this.servers.push(this.serverName);
    this.serverCreationStatus =
      'Server was created! Name is ' + this.serverName;
  }
  onUpdateServerName(event: Event) {
    //explicit casting:
    this.serverName = (<HTMLInputElement>event.target).value;
  }
  userNameEmptyString() {
    this.userName = null;
  }
  onToggleDetails() {
    this.showSecret = !this.showSecret;
    // this.log.push(this.log.length + 1);
    this.log.push(new Date());
  }
}
