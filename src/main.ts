import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic()
  .bootstrapModule(AppModule)
  .catch((err) => console.error(err));

// Angular starts this main.ts file first, from here we Bootstrap an Angular application and we pass the AppModule as an argument in this module we tell Angular in here that there is app.component which it should know when you try to start itself, angular analyses the app.component and reads the set up, knows the selector app-root now Angular can handle app-root in the index.html and it knows - this is the selector that it should know, as it is listed in the Bootstrap array in AppModule, so it should insert app.component, this has some html template attached to it and this is what is then rendered in the browser.
